import tkinter as tk
import tkinter.ttk as ttk
from functools import partial

window = tk.Tk()
window.title("Калькулятор")
window.minsize(width=250, height=250)
window.geometry(f"{window.winfo_screenwidth() // 4}x{window.winfo_screenheight() // 4}")

main_frame = ttk.Frame(window)

window.rowconfigure(0, weight=1)
window.columnconfigure(0, weight=1)
main_frame.grid(row=0, column=0, sticky="news")

text_box_var = tk.StringVar()
input_text_box = ttk.Entry(main_frame, textvariable=text_box_var, state='disabled')
input_text_box.grid(column=0, row=0, columnspan=5, sticky="news")
input_text_box.config(foreground='black')

left = '0'
right = '0'
action = ''
reset_on_next = False
dont_flush = False

digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
actions = ['+', '-', '*', '/']


def button_pressed(button_txt, *args):
    global left
    global right
    global action
    global reset_on_next
    global dont_flush

    if button_txt == '.':
        if action == '':
            if reset_on_next:
                left = '0.'
                reset_on_next = False
            elif float(left) == int(float(left)):
                left += '.'
        else:
            if float(right) == int(float(right)):
                right += '.'
        dont_flush = True

    if button_txt == 'CE':
        if action == '':
            left = '0'
        else:
            right = '0'
    if button_txt == '+/-':
        if action == '':
            left = str(-float(left))
        else:
            right = str(-float(right))
    if button_txt == 'C':
        left = '0'
        right = '0'
        action = ''
    if button_txt in digits:
        if action == '':
            if reset_on_next:
                left = button_txt
                reset_on_next = False
            else:
                if left[-1] == '.' and button_txt == '0':
                    dont_flush = True
                left += button_txt
        else:
            if right[-1] == '.' and button_txt == '0':
                dont_flush = True
            right += button_txt

    try:
        if button_txt in actions:
            if float(right) == 0:
                action = button_txt
            else:
                left = str(eval(left + action + right))
                action = button_txt
                right = '0'
        if button_txt == '=' and action != '':
            left = str(eval(left + action + right))
            action = ''
            right = '0'
            reset_on_next = True

        if float(left) == int(float(left)) and not dont_flush:
            left = str(int(float(left)))

        if float(right) == int(float(right)) and not dont_flush:
            right = str(int(float(right)))

        if float(right) == 0:
            text_box_var.set(left + action)
        else:
            text_box_var.set(left + action + right)
    except Exception:
        text_box_var.set("ошибка")
        left = '0'
        right = '0'
        action = ''

    dont_flush = False


def key_handler(arg):
    #print(arg.keysym)
    converter = {'plus': '+', 'minus': '-', 'slash': '/', 'asterisk': '*', 'Return': '=', 'period': '.', 'comma': '.',
                 'BackSpace': 'CE', 'Delete': 'C', 'Shift_R': '+/-', 'Shift_L': '+/-', 'equal': '='}
    if arg.keysym in converter:
        button_pressed(converter[arg.keysym])
    else:
        button_pressed(arg.keysym)

buttons_array_text = [['1', '2', '3', '+'], ['4', '5', '6', '-'], ['7', '8', '9', '*'], ['C', '0', '=', '/'],
                      ['CE', '.', '+/-', '']]
for i in range(1, 6):
    for j in range(4):
        btn_label = buttons_array_text[i - 1][j]
        bp = partial(button_pressed, button_txt=btn_label)
        b = ttk.Button(main_frame, text=btn_label, command=bp)
        b.grid(column=j, row=i, sticky="news", )

main_frame.columnconfigure(tuple(range(4)), weight=1)
main_frame.rowconfigure(tuple(range(6)), weight=1)

window.bind('<KeyPress>', key_handler)

window.mainloop()
